# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'ganges/product_taxonomy_service'

require 'minitest/autorun'
