# Ganges Platform Product Taxonomy Service

The authority on what product taxonomies are on the Ganges platform.
Allows for creation, validation and querying of product taxonomies

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake test` to run the tests. You can also run `bin/console`
for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/ganges/product_taxonomy_service.

## License

The gem is available as open source under the terms of the
[MIT License](https://opensource.org/licenses/MIT).
