# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ganges/product_taxonomy_service/version'

Gem::Specification.new do |spec|
  spec.name          = 'ganges-product_taxonomy_service'
  spec.version       = Ganges::ProductTaxonomyService::VERSION
  spec.authors       = ['Jeffrey Jones']
  spec.email         = ['jeff@jones.be']

  spec.summary       = 'gRPC service for the Ganges platform that is the authority on product taxonomy'
  spec.description   = '
	                        gRPC service for the Ganges platform that is the authorify on product taxonomy.
													It allows the creation, validation and querying of product taxonomies.
                         '
  spec.homepage      = 'https://gitlab.com/ganges/product_taxonomy_service'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'minitest', '~> 5.11'
  spec.add_development_dependency 'rake', '~> 12.3'
  spec.add_development_dependency 'rubocop', '~> 0.63.1'
end
