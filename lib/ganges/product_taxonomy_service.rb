# frozen_string_literal: true

require 'ganges/product_taxonomy_service/version'

module Ganges
  # The authority on Product Taxonomies for the Ganges Platform
  module ProductTaxonomyService
    # Your code goes here...
  end
end
